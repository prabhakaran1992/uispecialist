import React, {Component} from "react";
import {Col, Row,Button,Modal} from "antd";
import About from "../../../components/profile/About/index";
import Events from "../../../components/profile/Events/index";
import Contact from "../../../components/profile/Contact/index";
import Auxiliary from "../../../util/Auxiliary";
import ProfileHeader from "../../../components/profile/ProfileHeader/index";
import Speacilist from '../../components/dataEntry/Speacilist';
class Profile extends Component {
  state = { visible: false,loading:false };
  Modal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  render() {
    const{loading}=this.state
    return (
      <Auxiliary>
        <ProfileHeader/>
        <div className="gx-profile-content">
          <Row>
            <Col xl={16} lg={14} md={14} sm={24} xs={24}>
              <About/>
              
              <Events/>
            </Col>

            <Col xl={8} lg={10} md={10} sm={24} xs={24}>
              <Contact/>
              <Row>
                
                <div style={{margin:'auto'}}> 
               <Button onClick={this.Modal} type="primary">Book Appointment</Button>
               </div>
               <Modal
          title="Specialist"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>Return</Button>,
            <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
              Submit
            </Button>,
          ]}
        >
          <Speacilist />
        </Modal>
              </Row>
            </Col>
          </Row>
        </div>
      </Auxiliary>
    );
  }
}

export default Profile;


