import React, {Component} from "react";
import {Col, Row,Button,Modal} from "antd";
import About from "../../../components/profile/About/index";
import Events from "../../../components/profile/Events/index";
import Contact from "../../../components/profile/Contact/index";
import Auxiliary from "../../../util/Auxiliary";
import ProfileHeader from "../../../components/profile/ProfileHeader/index";
import Speacilist from '../../components/dataEntry/Speacilist';
class Userprofile extends Component {
  state = { visible: false,loading:false };
  Modal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  render() {
    const{loading}=this.state
    return (
      <Auxiliary>
        <ProfileHeader/>
        <div className="gx-profile-content">
          <Row>
            <Col xl={16} lg={14} md={14} sm={24} xs={24}>
              <About/>
              
              <Events/>
            </Col>

            <Col xl={8} lg={10} md={10} sm={24} xs={24}>
              <Contact/>
              <Row>
           
              </Row>
            </Col>
          </Row>
        </div>
      </Auxiliary>
    );
  }
}

export default Userprofile;


