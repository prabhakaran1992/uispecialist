export const testimonialsData = [
  {
    content: 'Religious wars are not caused by the fact that there is more than one religion, but by the spirit of intolerance... ',
    avatar: 'https://buffer.com/library/wp-content/uploads/2017/07/Facebook-cover-photo-1024x683.jpg',
    name: 'Montesquieu',
    title: 'Domnic'
  }, {
    content: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece, of classical Latin',
    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ5XqXST_TcfWhbJZy3fFp-NbTV9sWaBGdhD7sN_DY9i13yOoni',
    name: ' Brown',
    title: 'Domnic'
  }, {
      content: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. ',
      avatar: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAPDxAPDxAQDw8QDg8QDw8PDw8PDg8QFREWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGzIlHiYtLS0tLystKy0tLS0tLS0tLy0tLS0tLS0tLSstKy0rLSstLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAACAwABBAUGBwj/xAA2EAACAQIFAgUCBQMEAwEAAAABAgADEQQFEiExQVEGEyIyYXGBFCORodFCYrEHUsHwcoLhQ//EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAApEQACAgEEAgIBAwUAAAAAAAAAAQIRAwQSITETQQVRYSKx8BQjMnGR/9oADAMBAAIRAxEAPwDgRGLBEITiMhimMWKEYsQDVjFihGLEA5YxYpYxZIDlMchiFMasTAyFMKKUxgksQQliCJcQFyiJckAFMIthMgiLZY7AQwgERzCARKsYkiAyx5EBhKTAx2EWwmQwimEpMZjsIthMhhFMI7EJIgERxEWRGAuSWYMQBXl3g3kvEAV5RMq8omIRLyQbyQAJYQEpYQlFFiMWAIYiEGIxYCwxJAasasUsasQDFjVMSIxYgHKYwGJBhgxANEIRYMISRBiXKEKICiIBEZBIgAphFkR7CKYSrGLIgMIwwGjAUwimEc0W0pAJYRbCOMAiVYCGEWwjyIthGAhhAIjmEWRGAEku0logKgmFaVEIGSXaSABrDEBYYjGEIQgQhEAxY1YkRqmIBqxoiVMasQDFjBFiEIgGiMEUpjFiAMQxAEMSQCEMQRCEkRdpREKQwAWwimjmimlIBRizGNFmMYtoto1otpSAUYJjCIJEoBZEBhGkQSIwEMIthHsIDCMBBEq0aRK0wAXaCRHaYBEQC5UO0kBACGDFAwgYxjQYQMUDCBiAaDGKZjhoQaIDKVo1WmIrxivEBlhoYaYyvDDRAZKtGK0xlaMVomBkqYwGYytGq0lgPBhiJVowGIQySDeS8QFNFNGMYpjGgFtFmNImPXqonvdV/wDJgDKGk30UYBlU8TTa+lw1udO9pC/w36RPJFds6I6TNJWosoiVaXqH0+su0tNPoynjlB1JUARAIjSIJEogURFsI8iARGAi0lo0rKtABdoJWO0wSIAItJGESoAYMsR7YVl93pnS+EMqSsSRZiNrnpCm1aLhDczltJtex/SVqnoGfZNTVNNwD8CcHjcP5TWvcdCI6dWVkx7QNUsPEapNURkZIeMV5johIJHA5MfgMO9VgFUkE2vx+ke0aTfQ5XjA82mKyAIhOsq1tlYj1HtNLuNjyD1klTg49mUrRqtMRWjVaFEGWrRqtMRWjFaS0Blq0MNMUNDDyaEZQaXqmOHhobxUA3mYOMzBKdwPUw53soPyYGa5stNWp0rtVCnWy7rSXqSe84nG44t1J7dL/QTbDp5ZeekVFxXMlf4PRcBVwBI/GY9Rf/8ALDq+hT2NQDechnWCpVsw/D5WKmJWpp8rX73ci7AF7bDufmcyapMycBX0VUYmoNLXBpOEqA220sQQN7dJ149PHFbi+fyayyua2tUvwbvMfD+ZYIjzsNWo3OxUahcfKEjr1nTeGsRXq02/ELYAgI7LoZ++36bzp/Dnjr8fgKlDEkDGUkAD22rL3+DtYzW0muf5njfJayTvFKKv79o+i+H0tf3VJ19emJxGA6qLjqJp8bUFFlDbByQO1/8AidQxKrq5E1WeZYMVRIU2YepG/u/iedp8zx5FufB6Ou0kc2JpK36NfKIgYDD1lpAVlKupKne+oDhhGkT2seRT6PjtRp5YXyuxZEEiMMEiaGAu0lodpLQAC0FhG2gsIAIIlwyJIxDvEmkm44HMwvD+Y1KFTTRY+voJ1+ZeHVcEA/8A2Ny3I6OHCuFu47xxyqMeTsSNhgctarTL17k2uJ59WwtSpXqoqmyuQLja07/G+JCv5fpUWt8zW0nJvp02O5PUyd7uy21VGgqeGTZAH/MflbbCbXLPAt7Gq1/gbCbfDZhRp2IGpuDYXmyfNxa4VhtttM3KTZg4p+jnM4yXyF00EX5ueZqn/Iem2sawQWQdB1m7qUqldmcsybGwJ2nJYnAulZQSW1vYt8TbHJIuED1E5Bh8RhSCXQVLVCQ2o35B3/xNVjPA1AJYVyaji6uwFyQOLRj51SoU1UtcqgGld9gOs0Gc51XrqtaiD+SwIvtt2AjXP+jSUY++zm69M03ZDyrFT9jIrQcVjjXrFqirRZvcALAfNus3WAyIVFpt5jaarMqFUv7eTboIbXVnJLG74NWrRqtOnfwzQoka6jVTzpUqPT8npNJmVOhTuEuXNrAH0r3mW5N0S4NLkxg0MNMYNCDSqIMoPAxdVlpVCmzaDpPYnYRLObG1iegJsD94FWo/ltdQSQfaS1j02glyOjKzJ1wmDahQscRWphNra2W3qYn6Bt55u9yd73+eZ1+BzsA4p8Smp61HywoBGjqpFzsDdbEdpieG8h/GefWqVqdGlRUu+pWqE3BJ0i9xa3JPbmegpKMSlE5xVmYuEdbM1N1B4LKyqfpcT0X/AE2yHAVsTSq1BSq66tRBhnZiUdV1hip5sCBY3HJB2nsmf5NTxuFq4VgFV00qwUE0yPay/INpEsri6o0UFR85eHr0qgqDp7gL7qefr3+09DwGXvXIWkNTNa1us5TMMEMqx34TFkuAgYVMOVLFG1aSAfa1xwbfedx4Wz2iuFBpBhiUvTZzZGVAfSSouLkX/fmeb8lixznDI3S9nt/E6jJCEscVb9fz6MDP8HUw1UUSys4QsQDt0vbvzFYZrjeOx2Jes5JYm/c7znPGuJehh6fluVZ6ouy3DekE2v8AUCeNKMc+bZDi+j21klp8Lnl5o3tencEWuJpayWJHabDIMY2Iw1Kq/uZSGNrAlWIJ/aBmlO1m7kAytLN4s2x/dHJ8nhjn0/kX1aNbBMIyp7h8eDJLlQGSUZcowEBJLkjA7fzWU+o3jmr6hCApVDe4jjhVI2IE8yprpnSjks3wHmNcciLyzCVqZ/Mvp7TqBly32PWZpy1WAJPE3jPI1RTTNLg0UG4Am2VFtc/4jsLgqQbe1hH496bjQg45Mz8M3+pyFVGnxVK63HXia9snFVSXBDg+k34m9qYW4G+wmFicwo0WCueT1hi3wTpWxKTRgUfDiBTdrk9ze57TEzHLWFKy03DbbKfTt1nRCrTqLqTfsQZrsdiXpoz32HeaeecVUkXuOMzPJHYPWLFWQA6SLfYTfeHvENFKKLXZKTKdja91+RMUUa2MYamOm/TbaZFTw2pqh2C7AC3T4m0tRj21IV2bPMMywyoaiMK1/wCkGwJPTacy2VYiu96aO7MLgWNgTxqJ5G/7TfJ4dpUGR0YNexZLekHtOqwDAEWsNt/6fpIx5oRuio4t5w+Q5Gwr1KONouGpqCDTLeU1+lwOdxteZ2ZYXBUG0FShHILa6gO1r/7Ruf4nbXdLsNJuLHUW/Wa7DVMPXrVaT4dVe1zUX1CoeovYWabRyQk7srwbfR53muJomrajYLoF7cE77/4H2mL54BtvfsASf2npOLyEVNmFGnSAPpFIVCQe5bg/rMUeGcPh1106Xm1GFlCvop7d7kgcb2B54PEvdExnhV2eT+s1qzrocAAI1XQwUWI0ANcD3GY2X4UamD1BRF9NqjeWWJ3N129IF+3SbrxNk2JSsprKia2coiMgCgEcuP4HWarMcE7LTI1u6XDB/WQvIF+3PS03U01wyVE3fgbMHwdVK2qnUp0sQ7lBuGY0mp3FUDbZuDttOu8Rf6i5iykYGnQpbe5mNSr/AOoICg/W882zDNKlJxToUkRfLphkCAA1Les7c3P7WHSDhc2dAFekwt/cS36GS1K7siViszqNiapqVDVNdmu/ml2qNUta3G9+n2h4aticE1HE03ulUNdNWpToI1Kw6cj6faHiMVrqLVpO1Epvr23I4CjqfngTGr4kMUTX5dNEsosXJBYszcdSx7dJUoqUakjXHklBqUeGejUM2pVCiPZXYX9LCykqr2JNt7MOn3OxmRjMup100VQHXZtLdD37zyrMKivoBrFhTFkuDsPvxwJlZfmj0xYYutT7X1Ov73nk5Pjae7G6/wC/ue5h+ZuO3NG/59M9PpgIoRFCqoAVQLAAcAROYLrpkddj+hvOZXPmVabLW86pYLUDUyoNmY6htbcFR32M3S5ktRA1tLdVnBLSZcc03zyd0vkNPmwzS44fDMF+YMjtcyp7p8aQypZgxgSUZDKvACpJckACy3MqhOlSbfE3tPMG1qpY7wcmyTyHJbcEza4vLEexUbggycmKL5R2xSLr1HVdS7zGw+YVmO+w+tptadFiukiYGIy6prFuO0yhH0zScV2ZNBze7Ale44mf5qFCUmImJCL5Z+94VJQRZeDLWIhpGLWznyh6ht0+ZzXiTMBVpsQo+GnTY/KTUFtjaavF5YgARyPkTGWNwd3wQ4UB4HwTJRDuxYtwDxOqTCh9mUEdiLwvD1GmqKgtYDabbFUAo9PbexikvI91kdGHTyinp9Kqv02mBiMu8s7vYH5vAx2IOyq5Ruu/MxRSfZ2Jc9mMymoLhoOTLNAH089rf5jaFEklRckdhsZgVcTUBug3ta0RTzfFoCUGnuLAn9JhCt3KdFqcvRts0pYjy/LB8sE7sb3t1tNbgsrem1xU0kEG4tMzAs9anfFVagLh9Kg6bbXBPxNbisoWphq7LWNKqhUpbWRXF7WvcWv3nfDTp89FeSTN/hswqIwVXVy1rIeTc2AH32mj8U5jToDZXSo7EMtNiqautxuL/aairhsWAiis7KNw/p8xCDdbN7rXAN7zXZnXrUalNvMFVmDNerp2I5Di2xub8m/eaLBGX+DGsko9mxw2MR9JrIAx3G4uyg2IJI/7eZeC/DUKtWt5VXS4AIWpTrIFFzsosV+tvrM7K8AmJwgevh6K1XHuFREVt/cpG6n4+Jh4jInpAmjWQek6l1LUbbs3AH1F5lJS6YKUJdi84p4TE6DRtTdb2JUKpHPqIF78cmc14gwNTFVaWEpmirKjPtc61tyzKPjibXFJWSkqgVPzHd2c3JKgBR1490w8mqMmLeq6uAtMhHsbNbSrE/23tYjue4lQk02l6RnJL0h2F8AUKdFfxVQtUY6b+YERb22FuB9yZoc98N0KVlRmY2sgY7BdySW5sLrYWuSbTscuzdGpjEOl61PQrmoUZyWBIFNb+m+xJisuwjVMScQyWKmwoj0rYD5Nxv3mu6cFuf7lQx7jk8D/AKd4htGoAq9iXDXCg95u6n+nhpBgND6bH1BgW6ztfDOX1aVGnTqMoenSCswBIuCSAP1tx0m1w7trK1VsOPk/ImGXVqPbBpdI4PKfC9ADU9FxpCke4hz/AFbHgTYZvlq1aamggRkuNFtNx8zvK6EUyFTnh1HqH1moxuFDIbL6huD/ADKjnt0w8akjzSpTZDZhYiDN7muAdSKjqoQkXub3B6/E1GIenYBVIIO5vcGbrk5549omDJJAzKMqWYMYFySpIgOio5qapCjr1nTYDAMQN55dkGbrTqKH6melUszChXVh8iZ8wpM7oRtGzroafu4HWLoZjSvyDNRnHiZTSIHNjPMaviKotXSm4vv/ABNopPoJNo9Ux+CFZ9SNpmRg8iqFb6/pOP8AD+e+a4UnTfoZ3+EzBKQszbfWSuHTKfKtGOivTuri9h7p5jnWYMMTU9WwbYTuPFfilKaHy2DMe25nk+LxD1HZ9JuxuZE4buDKc1VHR4XxE6WsxH3m2oeLLg66j/Yzg1ouw1dunWUHI24mP9OkSmjqsZnrO40sQOhnYeHvEVHywlUhj/u6zyhL95lU6hHWTODqkxN2ew1cbhG9rgfWY1WrTUMU9TW9NjteeaYfHMu53HzCqZtUXdbqDxaZw3p8Ias7TDZuFJFYOlQ783B+x/4jsTmgLaS19Qsp4At8Tz7E5lUrW1kkjjebbC4lloWdVJ3K62Jb7KP5m8nI6oVXJ0NapW1aQEcDoXBNvod5g47BGv8AmU6YBW5bQdI43IUk2mlqY2rUZSzH07Km9lH15myyrFGiGqNUCIPcw23PQdzMrlHiLL/SzDxOZsvl0TRenRBOkqrrqJ36/O81+JzihVLABwm6l1UXuB0BIudx1HMDOs1fEliqFV6FrmoRc8n5vNG1MjoRfexvYfNp3Rk657ORyV8HUZf4oZSoqu3loPbTT1uw6HewEseLKnn1Ho0hpqC2moxJU+ncMtiPba1+puTOZ/BViRZHYHi1j+3ME4OoNirD4MKTdjc2kdomNXXTxFQYdKtwhps/rCCxD6gSX+/G83NDPdLqUCVFNy3kVEcjrvSPqP1E80TBv8LM/DYfTYliSOD2+kTiLzL2ez5HmFOo/pZWFRfSqsDve5sf+8TomoM25VduDPFMszLRU1O1n2K1bAOGsRu3O/zPQcu8Tg0h5zMhAFma4V/oeN5wZcSUqd0NVLmJ1Si4ILbDkDaYFcBvQnt6/wATSV/E2EC711ba5VNRb9O81eZ+NUClMOpvbTrbb9hNsUHVFOUY8tmv8Y5iHq+Qnsp7N/c85y8F6hYkk3JNyZV50pUqOOUnJ2w7yXgXl3jJCvKMq8omAEvJBvJADncRhze42mzweYVFAUsbS7SwsT5VM0WdobicSzrbiah8MQb2vNnaURGiPLK7NcmIZGDC4Imzq53XqgLc2imQdpWmFFPUS9Akk8kk/JlWh2l6ZRg3YsCC1IHn9Y7TL0wBNmOMMO5jEw47mOCwgItqK3MqiNH9KOL39YJP6iFix5n9Kpb/AG6v5lgQotiLWaaVWJweFs/uCXFg+5Cn5+Jn018sslfSzADS9KojqQd/coIMxpInjTLjqJIqmSOd99h0l121izWKjheg+glSo4wSJlllLstX00/KAGnXrGwuDYiwPaK7fG46wzAMpIhybdhvWY33tfchfSD9hFGXKMdIbbfYMNTAMsQEOBhACKBhAxNDsdqlhorVLDRUIdeS8XeXeAxgMsGKvJeIBl5RMAtKLQAO8kVqkjELtLtCtJIEVKtCktGSARKtGESrShAWlgQ7S7QADTL0w9Mu0YAASwIYEu0BggS7QrSWgAFpREZaDaMYEqGRBIgMEwDDMEwGDeUTLMqAAmVCMGMCwYQMXeVqiAdqlhom8sGFAZAaTVEgwgYhjdUom8G8mqAFyXg6oJMQgtUkVeXGBlWlWlyTMRUkuSMRVpdpJIxEtLtJJGIsS5JIwLtLkkgMkkuSAyoMkkYFGCZJIDBMEySQGCYMkkAKMEySRgCYJMkkAJeQNJJGAQaWGkkiALVL1SSRAUTAJkkgBV5JJIAf/9k=',
      name: 'Jeson Born',
      title: 'Director, Abc LLC'
    }, {
      content: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece, of classical Latin',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'John Smith',
      title: 'Chief Engineer'
    }, {
      content: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'Min Chan',
      title: 'Director, Abc LLC'
    }, {
      content: 'It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'Stella Johnson',
      title: 'Engineer Lead'
    }, {
      content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections ',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'Steve Smith',
      title: 'Director, Abc LLC'
    }
  ];
  export const basicTestimonialsData = [
    {
      content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'Alex Dolgove',
      title: 'BDM G-axon'
    }, {
      content: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'Domnic Brown',
      title: 'Product Head'
    }, {
      content: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'Jeson Born',
      title: 'Director, Abc LLC'
    }, {
      content: ' All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'John Smith',
      title: 'Chief Engineer'
    }, {
      content: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'Min Chan',
      title: 'Director, Abc LLC'
    }, {
      content: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'Stella Johnson',
      title: 'Engineer Lead'
    }, {
      content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections ',
      avatar: 'https://via.placeholder.com/150x150',
      name: 'Steve Smith',
      title: 'Director, Abc LLC'
    }
  ];
  