import React from "react";
import StarRatingComponent from "react-star-rating-component";
import {Avatar,Carousel} from "antd";
const Classic = () => {
 
  return (
    <Carousel autoplay>
      <div>
    <div className="gx-classic-testimonial gx-slide-item" style={{borderLeftStyle: 'inset'}}>
      <Avatar  src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ5XqXST_TcfWhbJZy3fFp-NbTV9sWaBGdhD7sN_DY9i13yOoni" alt="..."/>
      <h3 className="gx-title">Alex Dolgove</h3>
      <small className="gx-post-designation">BDM G-axon</small>
      <div className="gx-star-rating">
        <StarRatingComponent name="mathan" starCount={5} value={4.5}/>
      </div>
      <p className="gx-description">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
    </div>
    </div>

    <div>
    <div className="gx-classic-testimonial gx-slide-item" style={{borderLeftStyle: 'inset'}}>
      <Avatar src="https://buffer.com/library/wp-content/uploads/2017/07/Facebook-cover-photo-1024x683.jpg" alt="..."/>
      <h3 className="gx-title">Alex Dolgove</h3>
      <small className="gx-post-designation">BDM G-axon</small>
      <div className="gx-star-rating">
        <StarRatingComponent name="mathan" starCount={5} value={4.5}/>
      </div>
      <p className="gx-description">All the Lorem Ipsum generators on the Internet tend to repeat, making this the first true generator on the Internet</p>
    </div>
    </div>
   
    </Carousel>
  )
};
export default Classic;