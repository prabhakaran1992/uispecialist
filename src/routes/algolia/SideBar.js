import React from "react";
import { Layout, Slider,Checkbox } from "antd";
import {
  ClearRefinements,
  HierarchicalMenu,
  Panel,
  RangeInput,
  RatingMenu,
  RefinementList,
} from "react-instantsearch-dom";

const { Sider } = Layout;

function onChangecheck(e) {
  console.log(`checked = ${e.target.checked}`);
}

function onChange(value) {
  console.log('onChange: ', value);
}

function onAfterChange(value) {
  console.log('onAfterChange: ', value);
}

const Sidebar = () => (
  <Sider className="gx-algolia-sidebar">
    <div className="gx-algolia-sidebar-content">
      <ClearRefinements
        translations={{
          reset: 'Clear all filters',
        }}
      />
      <div className="gx-algolia-category-item">
        <div className="gx-algolia-category-title">Show results for</div>
        {/* <HierarchicalMenu
          attributes={['category', 'sub_category', 'sub_sub_category']}
        /> */}
      </div>
      <div className="gx-algolia-category-item">
        {/* <div className="gx-algolia-category-title">Refine By</div> */}

        {/* <Panel header={<span>Type</span>}>
          <RefinementList className="gx-algolia-refinementList" attribute="type" operator="or" limit={5} searchable/>
        </Panel> */}

        {/* <Panel header={<span>Materials</span>}>
          <RefinementList className="gx-algolia-refinementList"
                          attribute="materials"
                          operator="or"
                          limit={5}
                          searchable
          />
        </Panel> */}


        <Panel header={<span>Rating</span>}>
          <RatingMenu className="gx-algolia-refinementList" attribute="rating" max={5} />
        </Panel>
        <Panel header={<span>Gender</span>}>
        <Checkbox onChange={onChangecheck}>Male</Checkbox>
        <Checkbox onChange={onChangecheck}>Female</Checkbox>
        </Panel>
        <Panel header={<span>Consulting Price</span>}>
          <RangeInput className="gx-algolia-refinementList" attribute="price" />
        </Panel>
        <Panel header={<span>Slide</span>}>
        <Slider range
          step={10}
          defaultValue={[20, 50]}
          onChange={onChange}
          onAfterChange={onAfterChange} />
          </Panel>
      </div>
{/* 

      <div className="thank-you">
        Data courtesy of <a href="http://www.ikea.com/">ikea.com</a>
      </div> */}
    </div>
  </Sider>
);


export default Sidebar;

