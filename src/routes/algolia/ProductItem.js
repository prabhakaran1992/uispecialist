import React from "react";
import {Highlight,} from 'react-instantsearch-dom';
import StarRatingComponent from "react-star-rating-component";
import {Button} from 'antd';
import IntlMessages from "util/IntlMessages";
import {Link} from "react-router-dom";
const ProductItem = ({}) => {
  //console.log(item);
  const icons = [];
  // for (let i = 0; i < 5; i++) {
  //   const suffixClassName = i >= item.rating ? '--empty' : '';
  //   const suffixXlink = i >= item.rating ? 'Empty' : '';

  //   icons.push(
  //     <svg
  //       key={i}
  //       className={`ais-RatingMenu-starIcon ais-RatingMenu-starIcon${suffixClassName}`}
  //       aria-hidden="true"
  //       width="24"
  //       height="24"
  //     >
  //       <use xlinkHref={`#ais-RatingMenu-star${suffixXlink}Symbol`}/>
  //     </svg>
  //   );
  // }

  icons.push(
        <svg
          className={`ais-RatingMenu-starIcon ais-RatingMenu-starIcon`}
          aria-hidden="true"
          width="24"
          height="24"
        >
          </svg>
          );
  return (
    <div className="product">
    <div className="gx-product-item gx-product-vertical">
      <div className="gx-product-image">
        <img
          src={require("assets/images/doc.jpeg")} alt=''
        />
      </div>
      <div className="gx-product-body">
        <div className="gx-product-name">Dr.Ramanujam          
        </div>
        <div className="gx-mb-3">MBBS</div>
        <div className="ant-row-flex gx-mb-1">
          <StarRatingComponent
            name=""
            value={5}
            starCount={4}
            editing={false}/>
          <strong className="gx-d-inline-block gx-ml-2">5</strong>
        </div>
        <div className="gx-product-price">Rs. 1000</div>
        <div className="gx-product-footer">
        {/* <Button variant="raised"><IntlMessages
          id="eCommerce.addToCart"/></Button> */}
        <Button type="primary"></Button>
      </div>
      </div>
    </div>

<div className="gx-product-item gx-product-vertical">
      <div className="gx-product-image">
        <img
          src={require("assets/images/doc.jpeg")} alt=''
        />
      </div>
      <div className="gx-product-body">
        <div className="gx-product-name">Dr.Ramanujam
          
        </div>
        <div className="gx-mb-3">MBBS</div>
        <div className="ant-row-flex gx-mb-1">
          <StarRatingComponent
            name=""
            value={5}
            starCount={5}
            editing={false}/>
          <strong className="gx-d-inline-block gx-ml-2">5</strong>
        </div>
        <div className="gx-product-price">Rs. 1000</div>

      </div>
    </div>
    <div className="gx-product-item gx-product-vertical">
      <div className="gx-product-image">
        <img
          src={require("assets/images/doc.jpeg")} alt=''
        />
      </div>
      <div className="gx-product-body">
        <div className="gx-product-name">Dr.Ramanujam
          
        </div>
        <div className="gx-mb-3">MBBS</div>
        <div className="ant-row-flex gx-mb-1">
          <StarRatingComponent
            name=""
            value={5}
            starCount={5}
            editing={false}/>
          <strong className="gx-d-inline-block gx-ml-2">5</strong>
        </div>
        <div className="gx-product-price">Rs. 1000</div>

      </div>
    </div>
    </div>
    
  );
};

export default ProductItem;
