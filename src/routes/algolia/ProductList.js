import React from 'react';
import { Col, Row, Button, Card, Modal } from 'antd';
import IntlMessages from "util/IntlMessages";
import StarRatingComponent from "react-star-rating-component";
import ProductItem from './ProductItem';
import {Link} from "react-router-dom";
import Registration from '../components/dataEntry/Registration';
class ProductList extends React.Component {
  state = {visible: false};
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  render() {
    return (
    <div>
    <Row>
      <Col xl={8} lg={12} md={12} sm={12} xs={24}>
        <div className="product">
          <div className="gx-product-item gx-product-vertical">
            <Link to="/profile"><div className="gx-product-image">
              <img
                src={require("assets/images/stethoscope.jpg")} alt=''
              />
            </div></Link>
            <div className="gx-product-body">
              <div className="gx-product-name">Dr.Ramanujam </div>
              <small className=" gx-mb-1">MBBS</small>
              <div className="ant-row-flex gx-mb-1">
                <StarRatingComponent
                  name=""
                  value={5}
                  starCount={2}
                  editing={false} />
                {/* <strong className="gx-d-inline-block gx-ml-2">5</strong> */}
              </div>
              <div className="gx-product-price gx-mb-2">Rs. 1000</div>
              
                <Button onClick={this.showModal} type="primary">Book Appointment</Button>
              
            </div>
          </div>
        </div>
      </Col>
      <Col xl={8} lg={12} md={12} sm={12} xs={24}>
        <div className="product">
          <div className="gx-product-item gx-product-vertical">
          <Link to="/profile"><div className="gx-product-image">
              <img
                src={require("assets/images/doc.jpg")} alt=''
              />
            </div></Link>
            <div className="gx-product-body">
              <div className="gx-product-name">Dr.Muthaiya </div>
              <small className=" gx-mb-1">MBBS</small>
              <div className="ant-row-flex gx-mb-1">
                <StarRatingComponent
                  name=""
                  value={5}
                  starCount={4}
                  editing={false} />
              </div>
              <div className="gx-product-price gx-mb-2">Rs. 1000</div>
                <Button onClick={this.showModal} type="primary">Book Appointment</Button>
            
            </div>
          </div>
        </div>
      </Col>
      <Col xl={8} lg={12} md={12} sm={12} xs={24}>
        <div className="product">
          <div className="gx-product-item gx-product-vertical">
          <Link to="/profile"><div className="gx-product-image">
              <img
                src={require("assets/images/do.jpg")} alt=''
              />
            </div></Link>
            <div className="gx-product-body">
              <div className="gx-product-name">Dr.Muthaiya </div>
              <small className=" gx-mb-1">MBBS</small>
              <div className="ant-row-flex gx-mb-1">
                <StarRatingComponent
                  name=""
                  value={5}
                  starCount={4}
                  editing={false} />
              </div>
              <div className="gx-product-price gx-mb-2">Rs. 1000</div>
                <Button onClick={this.showModal} type="primary">Book Appointment</Button>
            
            </div>
          </div>
        </div>
      </Col>
      <Col xl={8} lg={12} md={12} sm={12} xs={24}>
        <div className="product">
          <div className="gx-product-item gx-product-vertical">
          <Link to="/profile"><div className="gx-product-image">
              <img
                src={require("assets/images/doc.jpg")} alt=''
              />
            </div></Link>
            <div className="gx-product-body">
              <div className="gx-product-name">Dr.Muthaiya </div>
              <small className=" gx-mb-1">MBBS</small>
              <div className="ant-row-flex gx-mb-1">
                <StarRatingComponent
                  name=""
                  value={5}
                  starCount={4}
                  editing={false} />
              </div>
              <div className="gx-product-price gx-mb-2">Rs. 1000</div>
                <Button onClick={this.showModal} type="primary">Book Appointment</Button>
            
            </div>
          </div>
        </div>
      </Col>
      <Col xl={8} lg={12} md={12} sm={12} xs={24}>
        <div className="product">
          <div className="gx-product-item gx-product-vertical">
          <Link to="/profile"><div className="gx-product-image">
              <img
                src={require("assets/images/do.jpg")} alt=''
              />
            </div></Link>
            <div className="gx-product-body">
              <div className="gx-product-name">Dr.Muthaiya </div>
              <small className=" gx-mb-1">MBBS</small>
              <div className="ant-row-flex gx-mb-1">
                <StarRatingComponent
                  name=""
                  value={5}
                  starCount={4}
                  editing={false} />
              </div>
              <div className="gx-product-price gx-mb-2">Rs. 1000</div>
                <Button onClick={this.showModal} type="primary">Book Appointment</Button>
            
            </div>
          </div>
        </div>
      </Col>
     
      <Col xl={8} lg={12} md={12} sm={12} xs={24}>
        <div className="product">
          <div className="gx-product-item gx-product-vertical">
          <Link to="/profile"><div className="gx-product-image">
              <img
                src={require("assets/images/stethoscope.jpg")} alt=''
              />
            </div></Link>
            <div className="gx-product-body">
              <div className="gx-product-name">Dr.Muthaiya </div>
              <small className=" gx-mb-1">MBBS</small>
              <div className="ant-row-flex gx-mb-1">
                <StarRatingComponent
                  name=""
                  value={5}
                  starCount={4}
                  editing={false} />
              </div>
              <div className="gx-product-price gx-mb-2">Rs. 1000</div>
                <Button onClick={this.showModal} type="primary">Book Appointment</Button>
            
            </div>
          </div>
        </div>
      </Col>
    </Row>
    
    <Modal
      title=""
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
    >
      <Registration/>
    </Modal>
    </div>
  );
};
}
export default ProductList;