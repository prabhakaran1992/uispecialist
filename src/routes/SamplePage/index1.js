import React, { Component } from "react";
import './sample.css';
import { Footer, AutoComplete, Form, message, Popover, Modal, Avatar, Dropdown, Menu, Select,Card,Layout, Cascader, Row, Col, Icon, Button, Tabs, DatePicker, Input, InputNumber } from
  "antd";
  import CustomScrollbars from "util/CustomScrollbars";
import { switchLanguage, toggleCollapsedSideNav } from "../../appRedux/actions/Setting";
import SearchBox from "components/SearchBox";
import UserInfo from "components/UserInfo";
import AppNotification from "components/AppNotification";
import MailNotification from "components/MailNotification";
import Testimonials from "../customViews/extras/testimonials/index";
import Callouts from '../customViews/extras/callouts/index'
import RoadMap from "components/Widgets/RoadMap";
import Speacilist from '../../routes/components/dataEntry/Speacilist';
import { Link } from "react-router-dom";
import Lawyer from 'components/Widgets/Lawyer';
import Lawyer2 from 'components/Widgets/lawyer2';
import Lawyer3 from 'components/Widgets/lawyer3';
import CardBox from "components/CardBox/index";
import { Carousel } from 'antd';
const { Header } = Layout;
const Option = Select.Option;
const FormItem = Form.Item;

function handleMenuClick(e) {
  message.info('Click on menu item.');
}

function handleChange(value) {
  console.log(`selected ${value}`);
}
class SamplePage1 extends Component {
  state = { visible: false, searchText: '' };
  Modal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };



  

  updateSearchChatUser = (evt) => {
    this.setState({
      searchText: evt.target.value,
    });
  };

  
  render() {
    const { locale, width, navCollapsed, navStyle } = this.props;
    const menu = (
      <Menu>
        <Menu.Item key="0">
          <li href="/sample">My Profile</li>
        </Menu.Item>
        <Menu.Item key="1">
          <li>LogOut</li>
        </Menu.Item>
      </Menu>
       );
    return (
      <div>
         <div className="gx-header-horizontal ">
        {/* <div className="gx-header-horizontal-top">
          <div className="gx-container">
            <div className="gx-header-horizontal-top-flex">
              <div className="gx-header-horizontal-top-left">
                <i className="icon icon-alert gx-mr-3" />
                <p className="gx-mb-0 gx-text-truncate" style={{ fontWeight: 'bold' }}>Specialist</p>
              </div>
              <ul style={{ margin: 'auto', fontWeight: 'bold', cursor: 'pointer' }}>
                <li onClick={this.Modal}>Are You Specialist</li>
              </ul>
              <ul className="gx-login-list">
                <Link to="/signin"><li>Login</li></Link>
                <Link to="/signup"><li>Signup</li></Link>
              </ul>
            </div>
          </div>
        </div> */}

        <Modal
          title="Specialist"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Speacilist />
        </Modal>
        <Header
          className="gx-header-horizontal-main">
          <div className="gx-container">
            <div className="gx-header-horizontal-main-flex">

              <div className="gx-d-block gx-d-lg-none gx-linebar gx-mr-xs-3">
                <i className="gx-icon-btn icon icon-menu"
                  onClick={() => {
                    this.props.toggleCollapsedSideNav(!navCollapsed);
                  }}
                />

              </div>
              <Link to="/" className="gx-d-block gx-d-lg-none gx-pointer gx-w-logo">
                <img alt="" src={require("assets/images/w-logo.png")} /></Link>
              <Link to="/" className="image">
                <img alt="" src={require("assets/images/logo.jpg")} /></Link>
              <div className="gx-header-search gx-d-none gx-d-lg-flex">
                <SearchBox styleName="gx-lt-icon-search-bar-lg"
                  placeholder="Search in app..."
                  onChange={this.updateSearchChatUser.bind(this)}
                  value={this.state.searchText}
                  style={{marginLeft:'10px'}} />
                  
                <Input
                styleName="gx-lt-icon-detect-lg"
                  placeholder="detect"
                  style={{width:120,marginLeft:'10px'}}
                />
                 <Link to="list" style={{marginLeft:'10px'}}><Button type="primary" style={{width:120}}>search</Button></Link>
              </div>

                <ul className="gx-header-notifications gx-ml-auto">
                  <li className="gx-notify gx-notify-search gx-d-inline-block gx-d-lg-none">
                    <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={
                      <div className="gx-d-flex">
                        <Dropdown overlay={menu}>
                          <Button>
                            Category <Icon type="down" />
                          </Button>
                        </Dropdown>
                        <SearchBox styleName="gx-popover-search-bar" z
                          placeholder="Search in app..."
                          onChange={this.updateSearchChatUser.bind(this)}
                          value={this.state.searchText} />
                      </div>
                    } trigger="click">
                      <span className="gx-pointer gx-d-block"><i className="icon icon-search-new" /></span>

                    </Popover>
                  </li>
                  <li className="gx-notify">
                    <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={<AppNotification />}
                      trigger="click">
                      <span className="gx-pointer gx-d-block"><i className="icon icon-notification" /></span>
                    </Popover>
                  </li>

                  <li className="gx-msg">
                    <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"
                      content={<MailNotification />} trigger="click">
                      <span className="gx-pointer gx-status-pos gx-d-block">
                        <i className="icon icon-chat-new" />
                        <span className="gx-status gx-status-rtl gx-small gx-orange" />
                      </span>
                    </Popover>
                  </li>
                 
                  <li className="gx-user-nav"><UserInfo /></li>
                </ul>
              </div>
            </div>
        </Header>
        </div>
        <Carousel autoplay>
          <div>
            <img src={require("assets/images/ca.jpg")} alt='' />
          </div>
          <div>
            <img src={require("assets/images/cb.jpg")} alt='' />
          </div>
          <div>
            <img src={require("assets/images/cc.jpg")} alt='' />
          </div>
          <div>
            <img src={require("assets/images/cd.jpg")} alt='' />
          </div>
        </Carousel>
        <Row>
          <div class="overlay-fade">
            <h2>Lawyer</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider.png")} alt='' />
            <div>
              <img src={require("assets/images/c1.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
          <div class="overlay-fade">
            <h2>Teacher</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider1.png")} alt='' />
            <div>
              <img src={require("assets/images/c2.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
          <div class="overlay-fade">
            <h2>Accountant</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider2.png")} alt='' />
            <div>
              <img src={require("assets/images/c3.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
          <div class="overlay-fade">
            <h2>Finanical</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider3.png")} alt='' />
            <div>
              <img src={require("assets/images/c4.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
          <div class="overlay-fade">
            <h2>Professor</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider4.png")} alt='' />
            <div>
              <img src={require("assets/images/c5.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
          <div class="overlay-fade">
            <h2>Engineer</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider4.png")} alt='' />
            <div>
              <img src={require("assets/images/c6.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
        </Row>
        {/* <div style={{"background":"url(../backgroundimage.jpg)",padding:'0px !important',backgroundAttachment:'fixed',width:'100%',height:'100%',backgroundRepeat:'no-repeat',marginTop: '-32px'}}>
        <div class="hide">
      
          <Tabs size="large" style={{ color: 'black' }} type="card">
            <TabPane tab="Search" key="1">
              <Row style={{ left: '6%', paddingTop: '15px', minWidth: '130%' }} span={8}>
               
                <Col span={5}>
                  <FormItem>
                    <Input icon="location" size="large" style={{ width: '110%' }} prefix={<Icon type="geo-location" />}
                      placeholder="Location" />
                  </FormItem>
                </Col>
                <Col span={5}>
                  <FormItem>
                    <Select placeholder="Categories" style={{ width: 250 }}
                      onChange={this.handleProvinceChange}>
                      {provinceData.map(province => (
                        <Option key={province}>{province}</Option>
                      ))}
                    </Select>
                  </FormItem>
                </Col>
                <Col span={5}>
                  <FormItem>
                    <Select style={{ width: 250 }} value={this.state.secondCity} onChange={this.onSecondCityChange}>
                      {cities.map(city => (
                        <Option key={city}>{city}</Option>
                      ))}
                    </Select>
                  </FormItem>
                </Col>
                <Col span={2}>
                  <FormItem>
                    <Link to="/list"><Button size="large" type="primary" icon="search" htmlType="submit">Search</Button></Link>
                  </FormItem>
                </Col>
              </Row>
            </TabPane>
          </Tabs>
          
      
        
        </div>
        </div> */}
         <div style={{ marginTop: '50px' }}>
          <CardBox styleName="gx-card-full" heading={'What about today--'}>
          <Testimonials />
          </CardBox>
        </div>
        <div style={{ marginTop: '50px' }}>
          <CardBox styleName="gx-card-full" heading={'Why use Specialist?'}>
          <Callouts />
          </CardBox>
        </div>
        <div style={{ marginTop: '50px' }}>
          <CardBox styleName="gx-card-full" heading={'My recommended Finds of the week - Lawyers'}>
          <Row>
            <Col span={6}>
              <RoadMap /> 
            </Col>
            <Col span={6}>
              <Lawyer />
            </Col>
            <Col span={6}>
              <Lawyer3 />
            </Col>
            <Col span={6}>
              <Lawyer2 />
            </Col>
          </Row>
          </CardBox >
        </div>
        <footer class="ftco-footer ftco-section">
          <div class="container">
            <div class="row">
              
            </div>
            <div class="row mb-5">
              <div class="col-md">
                <div class="ftco-footer-widget">
                  <h4 class="ftco-heading-2">Our Company</h4>
                  <div class="d-flex">
                    <ul class="list-unstyled">
                      <li>
                        <a href="#">About Us</a>
                      </li>
                      <li>
                        <a href="#">How It Works</a>
                      </li>
                      <li>
                        <a href="#">Affiliates</a>
                      </li>
                      <li>
                        <a href="#">Testimonials</a>
                      </li>
                      <li>
                        <a href="#">Contact Us</a>
                      </li>
                      <li>
                        <a href="#">Plan &amp; Pricing</a>
                      </li>
                      <li>
                        <a href="#">Blog</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <div class="col-md">
                <div class="ftco-footer-widget">
                  <h4 class="ftco-heading-2">Getting Started</h4>
                  <div class="d-flex">
                    <ul class="list-unstyled">
                      <li><a href="#">Contact</a></li>
                      <li><a href="#" >Join Specialist</a></li>
                      <li><a href="#" >Social Media & Blog</a></li>
                      <li><a href="#" >All Practices</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md">
                <div class="ftco-footer-widget mb-4 ftco-animate">
                  <h4 class="ftco-heading-2">Help Support</h4>
                  <div class="d-flex">
                    <ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
                      <li>
                        <a href="#">Support Forum</a>
                      </li>
                      <li>
                        <a href="#">Terms &amp; Conditions</a>
                      </li>
                      <li>
                        <a href="#">Support Policy</a>
                      </li>
                      <li>
                        <a href="#">Refund Policy</a>
                      </li>
                      <li>
                        <a href="#">FAQs</a>
                      </li>
                      <li>
                        <a href="#">Buyers Faq</a>
                      </li>
                      <li>
                        <a href="#">Sellers Faq</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md">
                <div class="ftco-footer-widget mb-4 ftco-animate">
                  <h4 class="ftco-heading-2">Legal</h4>
                  <div class="d-flex">
                    <ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
                      <li><a href="#">Cookie Policy</a></li>
                      <li><a href="#" >Privacy Policy</a></li>
                      <li><a href="#" >Terms & Conditions</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="gx-flex-row gx-justify-content-between">
                <ul className="gx-social-link">
                  <li>
                    <Icon type="google" onClick={() => {
                      this.props.showAuthLoader();
                      this.props.userGoogleSignIn();
                    }} />
                  </li>
                  <li>
                    <Icon type="facebook" onClick={() => {
                      this.props.showAuthLoader();
                      this.props.userFacebookSignIn();
                    }} />
                  </li>
                  <li>
                    <Icon type="github" onClick={() => {
                      this.props.showAuthLoader();
                      this.props.userGithubSignIn();
                    }} />
                  </li>
                  <li>
                    <Icon type="twitter" onClick={() => {
                      this.props.showAuthLoader();
                      this.props.userTwitterSignIn();
                    }} />
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}
const mapStateToProps = ({ settings }) => {
  const { locale, navStyle, navCollapsed, width } = settings;
  return { locale, navStyle, navCollapsed, width }
};
export default SamplePage1;