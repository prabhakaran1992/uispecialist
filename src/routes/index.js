import React from "react";
import {Route, Switch} from "react-router-dom";
import SocialApps from "./socialApps/index";
import asyncComponent from "util/asyncComponent";

const App = ({match}) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}sample`} component={asyncComponent(() => import('./SamplePage'))}/>
      <Route path={`${match.url}sample1`} component={asyncComponent(() => import('./SamplePage/index1'))}/>
      <Route path={`${match.url}list`} component={asyncComponent(() => import('./algolia'))}/> 
      <Route path={`${match.url}appointment`} component={asyncComponent(() => import('./inBuiltApps/Todo/index'))}/>     
      <Route path={`${match.url}profile`} component={asyncComponent(() => import('./socialApps/Profile/index'))}/>
      <Route path={`${match.url}dashboard`} component={asyncComponent(() => import('./main/dashboard/CRM/index'))}/>
      <Route path={`${match.url}userprofile`} component={asyncComponent(() => import('./socialApps/Profile/userprofile'))}/>
      
    </Switch>
  </div>
);

export default App;
