import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Checkbox, Form, Icon, Input, Layout, message, Popover, Modal, Avatar, Dropdown, Menu, Select } from "antd";
import CustomScrollbars from "util/CustomScrollbars";
import languageData from "./languageData";
import { switchLanguage, toggleCollapsedSideNav } from "../../appRedux/actions/Setting";
import SearchBox from "components/SearchBox";
import UserInfo from "components/UserInfo";
import AppNotification from "components/AppNotification";
import MailNotification from "components/MailNotification";
import Auxiliary from "util/Auxiliary";
import IntlMessages from "../../util/IntlMessages";
import { Divider } from 'antd';
import { NAV_STYLE_DRAWER, NAV_STYLE_FIXED, NAV_STYLE_MINI_SIDEBAR, TAB_SIZE } from "../../constants/ThemeSetting";
import { connect } from "react-redux";
import Speacilist from '../../routes/components/dataEntry/Speacilist';
import image from "../../assets/images/do.jpg";
import './topbar.css'
const { Header } = Layout;
const Option = Select.Option;
const FormItem = Form.Item;

function handleMenuClick(e) {
  message.info('Click on menu item.');
}

function handleChange(value) {
  console.log(`selected ${value}`);
}

class Topbar extends Component {


  state = { visible: false, searchText: '' };
  Modal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };



  languageMenu = () => (
    <CustomScrollbars className="gx-popover-lang-scroll">
      <ul className="gx-sub-popover">
        {languageData.map(language =>
          <li className="gx-media gx-pointer" key={JSON.stringify(language)} onClick={(e) =>
            this.props.switchLanguage(language)
          }>
            <i className={`flag flag-24 gx-mr-2 flag-${language.icon}`} />
            <span className="gx-language-text">{language.name}</span>
          </li>
        )}
      </ul>
    </CustomScrollbars>);

  updateSearchChatUser = (evt) => {
    this.setState({
      searchText: evt.target.value,
    });
  };


  render() {
    // const { locale, width, navCollapsed, navStyle } = this.props;
    // const menu = (
    //   <Menu>
    //     <Menu.Item key="0">
    //       <li href="/sample1">My Profile</li>
    //     </Menu.Item>
    //     <Menu.Item key="1">
    //       <li>LogOut</li>
    //     </Menu.Item>
    //   </Menu>
    // );
    return (<div></div>
      // <div className="gx-header-horizontal ">
      //   <div className="gx-header-horizontal-top">
      //     <div className="gx-container">
      //       <div className="gx-header-horizontal-top-flex">
      //         <div className="gx-header-horizontal-top-left">
      //           <i className="icon icon-alert gx-mr-3" />
      //           <p className="gx-mb-0 gx-text-truncate" style={{ fontWeight: 'bold' }}>Specialist</p>
      //         </div>
      //         <ul style={{ margin: 'auto', fontWeight: 'bold', cursor: 'pointer' }}>
      //           <li onClick={this.Modal}>Are You Specialist</li>
      //         </ul>
      //         <ul className="gx-login-list">
      //           <Link to="/signin"><li>Login</li></Link>
      //           <Link to="/signup"><li>Signup</li></Link>
      //         </ul>
      //       </div>
      //     </div>
      //   </div>

      //   <Modal
      //     title="Specialist"
      //     visible={this.state.visible}
      //     onOk={this.handleOk}
      //     onCancel={this.handleCancel}
      //   >
      //     <Speacilist />
      //   </Modal>
      //   <Header
      //     className="gx-header-horizontal-main">
      //     <div className="gx-container">
      //       <div className="gx-header-horizontal-main-flex">

      //         <div className="gx-d-block gx-d-lg-none gx-linebar gx-mr-xs-3">
      //           <i className="gx-icon-btn icon icon-menu"
      //             onClick={() => {
      //               this.props.toggleCollapsedSideNav(!navCollapsed);
      //             }}
      //           />

      //         </div>
      //         <Link to="/" className="gx-d-block gx-d-lg-none gx-pointer gx-w-logo">
      //           <img alt="" src={require("assets/images/w-logo.png")} /></Link>
      //         <Link to="/" className="image">
      //           <img alt="" src={require("assets/images/logo.jpg")} /></Link>
      //         <div className="gx-header-search gx-d-none gx-d-lg-flex">
      //           <SearchBox styleName="gx-lt-icon-search-bar-lg"
      //             placeholder="Search in app..."
      //             onChange={this.updateSearchChatUser.bind(this)}
      //             value={this.state.searchText} />
      //           <Input
      //           styleName="gx-lt-icon-detect-lg"
      //             placeholder="detect"
      //             style={{width:120}}
      //           />
      //             <Select defaultValue="lucy" style={{ width: 120 }} onChange={handleChange}>
      //               <Option value="jack">Products</Option>
      //               <Option value="lucy">Apps</Option>
      //               <Option value="Yiminghe">Blogs</Option>
      //             </Select>
      //         </div>

      //           <ul className="gx-header-notifications gx-ml-auto">
      //             <li className="gx-notify gx-notify-search gx-d-inline-block gx-d-lg-none">
      //               <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={
      //                 <div className="gx-d-flex">
      //                   <Dropdown overlay={menu}>
      //                     <Button>
      //                       Category <Icon type="down" />
      //                     </Button>
      //                   </Dropdown>
      //                   <SearchBox styleName="gx-popover-search-bar" z
      //                     placeholder="Search in app..."
      //                     onChange={this.updateSearchChatUser.bind(this)}
      //                     value={this.state.searchText} />
      //                 </div>
      //               } trigger="click">
      //                 <span className="gx-pointer gx-d-block"><i className="icon icon-search-new" /></span>

      //               </Popover>
      //             </li>
      //             <li className="gx-notify">
      //               <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={<AppNotification />}
      //                 trigger="click">
      //                 <span className="gx-pointer gx-d-block"><i className="icon icon-notification" /></span>
      //               </Popover>
      //             </li>

      //             <li className="gx-msg">
      //               <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"
      //                 content={<MailNotification />} trigger="click">
      //                 <span className="gx-pointer gx-status-pos gx-d-block">
      //                   <i className="icon icon-chat-new" />
      //                   <span className="gx-status gx-status-rtl gx-small gx-orange" />
      //                 </span>
      //               </Popover>
      //             </li>
      //             <li className="gx-language">
      //               <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"
      //                 content={this.languageMenu()} trigger="click">
      //                 <span className="gx-pointer gx-flex-row gx-align-items-center"><i
      //                   className={`flag flag-24 flag-${locale.icon}`} />
      //                 </span>
      //               </Popover>
      //             </li>
      //             <li className="gx-user-nav"><UserInfo /></li>
      //           </ul>
      //         </div>
      //       </div>
      //   </Header>
      //   </div>

    );
  }
}

const mapStateToProps = ({ settings}) => {
  const { locale, navStyle, navCollapsed, width} = settings;
  return { locale, navStyle, navCollapsed, width}
};

export default connect(mapStateToProps, { toggleCollapsedSideNav, switchLanguage})(Topbar);
